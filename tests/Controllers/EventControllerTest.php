<?php

namespace Tests\Controllers;

use App\Models\Event;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class EventControllerTest extends TestCase
{

    /**
     * A test to get all Events
     *
     * @return void
     */

    public function eventIndexReturnsValidData()
    {
        $this
            ->json('get', 'api/event')
            ->assertStatus(ResponseAlias::HTTP_OK)
            ->assertJsonStructure([
                'code',
                'message',
                'events' => [
                    '*' => [
                        'id',
                        'title',
                        'description',
                        'start_date',
                        'end_date',
                        'created_at',
                    ]
                ],
            ])
        ;
    }

    /**
     * A test to create a new Event
     *
     * @return void
     */

    public function eventStoreReturnValidDataStructure()
    {
        // Fake Start Date
        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');

        // Fake End Date that makes sure that end_date is after start_date
        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');


        // Fake data to test create event.
        $data = [
            'title' => fake()->text(250),
            'description' => fake()->paragraph(),
            'start_date' => $start,
            'end_date' => $end,
        ];
        $this->json('post', 'api/event', $data)
            ->assertStatus(ResponseAlias::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'message',
                    'data' => [
                        'id',
                        'title',
                        'description',
                        'start_date',
                        'end_date',
                        'created_at',
                    ]
                ]
            );

        // Make sure that the event has been added to database.
        $this->assertDatabaseHas('events', $data);
    }


    public function eventShowReturnsValidData() {
        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');
        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');
        $event = Event::create(
            [
                'title' => fake()->text(250),
                'description' => fake()->paragraph(),
                'start_date' => $start,
                'end_date' => $end,
            ]
        );


        $this->json('get', "api/event/$event->id")
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'data' => [
                        'id' => $event->id,
                        'title' => $event->title,
                        'description'  => $event->description,
                        'start_date'  => $event->start_date,
                        'end_date'  => $event->end_date,
                        'created_at' => $event->created_at,
                    ]
                ]
            );
    }


    public function eventUpdateReturnValidData() {

        // Fake Start Date
        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');

        // Fake End Date that makes sure that end_date is after start_date
        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');


        // Fake data to test create event.
        $data = [
            'title' => fake()->text(250),
            'description' => fake()->paragraph(),
            'start_date' => $start,
            'end_date' => $end,
        ];

        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');

        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');

        $event = Event::create(
            [
                'title' => fake()->text(250),
                'description' => fake()->paragraph(),
                'start_date' => $start,
                'end_date' => $end,
            ]
        );

        $this->json('put', "api/event/$event->id", $data)
            ->assertStatus(ResponseAlias::HTTP_OK)
            ->assertExactJson(
                [
                    'data' => [
                        'id' => $event->id,
                        'title' => $event->title,
                        'description'  => $event->description,
                        'start_date'  => $event->start_date,
                        'end_date'  => $event->end_date,
                        'created_at' => $event->created_at,
                    ]
                ]
            );
    }

    public function eventIsDestroyed()
    {
        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');

        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');

        $data = [
            'title' => fake()->text(250),
            'description' => fake()->paragraph(),
            'start_date' => $start,
            'end_date' => $end,
        ];

        $event = Event::create(

        );

        $this->json('delete', "api/event/$event->id")
            ->assertNoContent();
        $this->assertDatabaseMissing('events', $data);
    }

}
