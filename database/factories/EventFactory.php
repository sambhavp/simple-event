<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $start = fake()->dateTimeBetween('-2 weeks', '2 weeks');
        $end = fake()->dateTimeBetween($start,  $start->format('Y-m-d H:i:s').' +1 month');

        return [
            'title' => fake()->text(250),
            'description' => fake()->paragraph(),
            'start_date' => $start,
            'end_date' => $end,
        ];
    }
}
