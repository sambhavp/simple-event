import 'bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import { createApp } from 'vue/dist/vue.esm-bundler';
import App from "./application/components/App.vue";
import {router} from "./application/router/index";
import axios from "axios";
import VueAxios from "vue-axios";

const app = createApp(App)
app.use(router)
app.use(VueAxios, axios)
app.mount('#app')
