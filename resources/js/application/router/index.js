import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router'

const Home = () => import('@/application/components/Home')

const Events = () => import('@/application/components/events/List')
const Event = () => import('@/application/components/events/Show')
const EditEvent = () => import('@/application/components/events/Edit')
const CreateEvent = () => import('@/application/components/events/Create')

const routes = [
    {
        path: '/',
        component: Home,
        name: 'Home',
        children: {
                path: 'event',
                meta: { label: 'Events' },
                component: {
                    render(c) { return c('router-view') }
                },
                children: [
                    {
                        path: '/events',
                        name: 'Events',
                        component: Events,
                    },
                    {
                        path: 'create',
                        meta: { label: 'Create Event' },
                        name: 'Create Event',
                        component: CreateEvent
                    },
                    {
                        path: ':id',
                        meta: { label: 'Event Details' },
                        name: 'Event',
                        component: Event,
                    },
                    {
                        path: ':id/edit',
                        meta: { label: 'Edit Event' },
                        name: 'Edit Event',
                        component: EditEvent
                    }
                ]
            },
    }
];

export const router = createRouter({
    history: createWebHistory(),
    routes
})

