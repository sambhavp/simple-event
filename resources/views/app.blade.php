<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Simple Event</title>
    @vite('resources/css/app.css')
</head>
<body class="antialiased">
    <noscript>
        <strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app">
        <main-component></main-component>
    </div>
    @vite('resources/js/app.js')
</body>
</html>
